﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNet_MVC_42.Controllers
{
    public class CodingIDController : Controller
    {
        // GET: CodingID
        public ActionResult CodingIDMessages()
        {
            ViewBag.Message = "Learn, Code Share";
            return View();
        }
    }
}